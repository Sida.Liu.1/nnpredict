#!/bin/bash
# runs parse_data on /generation00000 - generation00026 to retrieve .tfrecord files
for i in {0..26}
do
	if [ $i -lt 10 ]
	then
  		generation="/generation0000"
  		generation="${generation}${i}"
	else    
  		generation="/generation000"
  		generation="${generation}${i}"
	fi
	python parse_data.py $generation
done
