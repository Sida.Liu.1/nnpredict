# NNPredict

This is a course project for CS395A Deep Learning.

In this project, we use deep neural networks to predict if a profile of soft robot can move or not.

We use simulation to train the neural networks.

![Robot](https://github.com/liusida/gpuVoxels/blob/dev-CUDA-0.1/doc/misc/bigGuys_small.png?raw=true)
