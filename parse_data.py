import numpy as np
import xml.etree.ElementTree as ET
import tensorflow as tf
import sys
#generation = '/generation00000'

def parse_output(root_dir):
    tree = ET.parse(root_dir+generation+'/output.xml')
    root = tree.getroot()
    det = root.find('detail')
    dists = []
    for child in det:
        dists.append((child.tag, float(child.find('distance_by_size').text)))
    return dists

def get_best(root_dir):
    tree = ET.parse(root_dir+generation+'/output.xml')
    root = tree.getroot()
    best = root.find('bestfit')
    for child in best:
        print(child.tag, child.text)

def parse_input(root_dir, file_list):
    data = []
    root = ET.parse(root_dir+generation+ '/base.vxa')
    struct = root.find('VXC').find('Structure')
    x_d = int(struct.find('X_Voxels').text)
    y_d = int(struct.find('Y_Voxels').text)
    z_d = int(struct.find('Z_Voxels').text)
    for r_id, y in file_list:
        phase = []
        with open(root_dir+generation+'/'+r_id+'.vxd') as f:
            s = f.readline()
        root = ET.fromstring(s)
        # Get phase offset data
        struct = root.find('Structure')
        phase_data = struct.find('PhaseOffset')
        for child in phase_data:
            phase.append([float(x) for x in child.text.split(',')])
        """
        # Get tissue composition
        # This is redundant for robots with no passive tissue
        mask = []
        data = struct.find('Data')
        for child in data:
            mask.append([int(x) for x in child.text])
        """
        data.append((np.array(phase).reshape((x_d, y_d, z_d)), y))
    return data

def save_to_TFRecord(root_dir, data):
    tf.compat.v1.enable_eager_execution()

    def _tensor_feature(value):
        value = tf.io.serialize_tensor(value)
        if isinstance(value, type(tf.constant(0))):
            value = value.numpy()
        return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

    def _float_feature(value):
        return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))

    def serialize_example(x, y):
        feature = {
            'x' : _tensor_feature(x),
            'y' : _float_feature(y),
        }
        example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
        return example_proto.SerializeToString()

    with tf.io.TFRecordWriter(root_dir+generation+'.tfrecord') as writer:
        for x, y in data:
            example = serialize_example(x, y)
            writer.write(example)

if __name__=='__main__':
    generation = str(sys.argv[1])
    root_dir = 'v2_data'
    get_best(root_dir)
    y = parse_output(root_dir)
    data = parse_input(root_dir, y)
    save_to_TFRecord(root_dir, data)
